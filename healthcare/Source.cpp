#include "Patient.h"
#include <iostream>
#include "Day.h"

using namespace std;

//global variables
Patient* patients[10] = {nullptr};

//forward declarations
void showMainMenu();
void addPatient();
void displayPatientDetails();

int main()
{
	showMainMenu();


	system("pause");
	return 0;
}

void showMainMenu()
{
	cout << "1. Add a new patient" << endl;
	cout << "2. Display patient details" << endl;

	int selection;
	cin >> selection;

	switch (selection)
	{
	case 1:
		addPatient();
		break;
	case 2:
		displayPatientDetails();
		break;
	default:
		break;
	}
}

void addPatient()
{
	//create a new patient
	Patient patient;

	//get patient details
	cout << "Enter the patient's first name" << endl;
	string firstName;
	cin >> firstName;

	cout << "Enter the patient's last name" << endl;
	string lastName;
	cin >> lastName;

	//generate a patient Id
	int id = rand() % 1000 + 1;

	//populate patient object
	patient.setFirstName(firstName);
	patient.setLastName(lastName);
	patient.setId(id);

	//add patient to array
	for(int i=0; i < 10; i++)
	{
		if(patients[i] == nullptr)
		{
			patients[i] = &patient;
			break;
		}
	}

	//indicate successful creation
	char* pId;
	cout << patient.getFirstName() + " " + patient.getLastName() + "'s ID is " << patient.getId();
	cout << endl << endl;

	//back to main menu
	system("pause");

	showMainMenu();
}

void displayPatientDetails()
{
	cout << "Please enter the patient's ID > ";

	int selection;
	cin >> selection;

	for (int i = 0; i < 10; i++)
	{
		if (patients[i] != nullptr && patients[i]->getId() == selection)
		{
			cout << "Patient Name: " + patients[i]->getFirstName() + " " + patients[i]->getLastName() << endl << endl;
		}
	}

	//back to main menu
	system("pause");

	showMainMenu();
}