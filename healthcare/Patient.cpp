#include "Patient.h"

Patient::Patient(void)
{
}

Patient::~Patient(void)
{
}

string Patient::getFirstName()
{
	return this->firstName;
}

void Patient::setFirstName(string name)
{
	this->firstName = name;

	//do formatting
	this->firstName[0] = toupper(this->firstName[0]);

	for (int i = 1; i < this->firstName.length(); i++)
	{
		this->firstName[i] = tolower(this->firstName[i]);
	}
}

string Patient::getLastName()
{
	return this->lastName;
}

void Patient::setLastName(string name)
{
	this->lastName = name;

	//do formatting
	this->lastName[0] = toupper(this->lastName[0]);

	for (int i = 1; i < this->lastName.length(); i++)
	{
		this->lastName[i] = tolower(this->lastName[i]);
	}
}

int Patient::getId()
{
	return this->Id;
}

void Patient::setId(int id)
{
	this->Id = id;
}

Record Patient::getRecord()
{
	return this->record;
}

void Patient::setRecord(Record rec)
{
	this->record = rec;
}