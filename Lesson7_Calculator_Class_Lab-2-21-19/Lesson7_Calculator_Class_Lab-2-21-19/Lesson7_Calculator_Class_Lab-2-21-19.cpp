// Lesson7_Calculator_Class_Lab-2-21-19.cpp : Defines the entry point for the console application.
/*Create a class named Calculator.  The class should have the following static  functions:

int add(int, int) - this function should take 2 numbers and return the sum
int subtract(int, int) - this function should take 2 numbers and subtract the first number from the second
int multiply(int, int) - this function should take 2 numbers and return the product of them
double divide(int, int) - this function should take 2 numbers and divide the first by the second, returning the quotient.*/

#include "stdafx.h"
#include "Calculator.h"
#include <iostream>
#include <vector>
#include "Person.h"

using namespace std;


int _tmain(int argc, _TCHAR* argv[])
{
	/*Calculator<int> intCalc;
	Calculator<double> doubleCalc;

	int sum = intCalc.add(2, 2);
	double sumD = doubleCalc.add(2.0, 2.0);*/

	Person jack("Jack", "AndtheBeanStalk");
	Person diane("Diane", "PrincessOfWales");

	vector<Person> people;
	vector<Person>::iterator it;

	people[0] = jack;
	people[1] = diane;

	for(int i = 0; i < people.size(); i++)
		cout << people[i].getName() << endl;

	for(it = people.begin(); it != people.end(); it++)
		cout << it->getName() << endl;


	return 0;
}

