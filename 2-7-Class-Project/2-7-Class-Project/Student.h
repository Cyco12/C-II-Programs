#pragma once

#include <string>

using namespace std;

class Student
{
private:
	int studentNumber;
	string name;

protected:
	string studentRec;

public:
	Student(string);
	~Student(void);

	int getStudentNumber(){return studentNumber;}
	void setStudentNumber(int number){ this->studentNumber = number;}
};

