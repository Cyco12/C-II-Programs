#pragma once

#include <string>

using namespace std;


class Person
{
public:
	Person(string, string);
	~Person(void);

	string getName();
	void setName(string, string);

private:
	string firstName;
	string lastName;
};
