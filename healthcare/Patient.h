#pragma once

#include <string>

using namespace std;

struct Visit
{
	int year;
	int month;
	int day;

	string reason;
	string diagnosis;
};

struct Record
{
	Visit visit[10];
	string status;
};

class Patient
{
public:
	Patient();
	~Patient(void);

	//getter/setters
	string getFirstName();
	string getLastName();
	int getId();
	Record getRecord();

	void setFirstName(string);
	void setLastName(string);
	void setId(int);
	void setRecord(Record);

private:
	string firstName;
	string lastName;

	int Id;

	Record record;
};

